package org.magenpurp.onfimarist.ecospawners.files;

import org.magenpurp.api.utils.FileManager;

public class PricesFile extends FileManager {
    public static final String spawners = "Spawners.";

    public static final String name = ".Name";
    public static final String price = ".Price";

    public PricesFile() {
        super("prices");
        addDefault(spawners + ".Creeper" + name, "Creeper");
        addDefault(spawners + ".Creeper" + price, 1.5);
        copyDefaults();
        save();
    }

    public boolean isCached(String spawnerName) {
        return contains("Spawners." + spawnerName);
    }

}
