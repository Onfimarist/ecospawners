package org.magenpurp.onfimarist.ecospawners;

import de.dustplanet.silkspawners.events.SilkSpawnersSpawnerBreakEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class Events implements Listener {

    @EventHandler
    public void SilkSpawnersSpawnerBreakEvent(SilkSpawnersSpawnerBreakEvent e) {
        Player p = e.getPlayer();
        String name = e.getSpawner().getCreatureTypeName();

        if (!Main.getEconomy().hasAccount(p)) {
            Bukkit.getLogger().info("Vault account not found for " + p.getDisplayName());
            return;
        }

        switch(name) {
            case ("creeper"):
                if (Main.getEconomy().getBalance(p) > 125000) {
                    Main.getEconomy().withdrawPlayer(p, 12500);
                    break;
                }
                p.sendMessage("Not enough muggle money.");
                break;
            case ("pig"):
                if (Main.getEconomy().getBalance(p) > 17500) {
                    Main.getEconomy().withdrawPlayer(p, 17500);
                    break;
                }
                p.sendMessage("Not enough muggle money.");
                break;
            case ("chicken"):
                if (Main.getEconomy().getBalance(p) > 17500) {
                    Main.getEconomy().withdrawPlayer(p, 12500);
                    break;
                }
                p.sendMessage("Not enough muggle money");
                break;
            case ("zombie"):
            case ("sheep"):
            case ("spider"):
                if (Main.getEconomy().getBalance(p) > 25000) {
                    Main.getEconomy().withdrawPlayer(p, 25000);
                    break;
                }
                p.sendMessage("Not enough muggle money");
                break;
            case ("ocelot"):
            case ("cow"):
                if (Main.getEconomy().getBalance(p) > 35000) {
                    Main.getEconomy().withdrawPlayer(p, 35000);
                    break;
                }
                p.sendMessage("Not enough muggle money");
                break;
            case ("skeleton"):
                if (Main.getEconomy().getBalance(p) > 37500) {
                    Main.getEconomy().withdrawPlayer(p, 37500);
                    break;
                }
                p.sendMessage("Not enough muggle money");
                break;
            case ("wolf"):
                if (Main.getEconomy().getBalance(p) > 50000) {
                    Main.getEconomy().withdrawPlayer(p, 50000);
                    break;
                }
                p.sendMessage("Not enough muggle money");
                break;
            case ("blaze"):
                if (Main.getEconomy().getBalance(p) > 200000) {
                    Main.getEconomy().withdrawPlayer(p, 200000);
                    break;
                }
                p.sendMessage("Not enough muggle money");
                break;
            case ("enderman"):
                if (Main.getEconomy().getBalance(p) > 212500) {
                    Main.getEconomy().withdrawPlayer(p, 212500);
                    break;
                }
                p.sendMessage("Not enough muggle money");
                break;
            case ("villager"):
                if (Main.getEconomy().getBalance(p) > 237500) {
                    Main.getEconomy().withdrawPlayer(p, 237500);
                    break;
                }
                p.sendMessage("Not enough muggle money");
                break;
            default:
                if (Main.debugger) {
                    p.sendMessage("Unknown spawner: " + name + " debugger lol");
                    break;
                }
        }
    }
}