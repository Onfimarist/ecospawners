package org.magenpurp.onfimarist.ecospawners.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.magenpurp.api.command.SubCommand;

public class ReloadSubCommand extends SubCommand {

    public ReloadSubCommand(String name, String[] permissions) {
        super(name, permissions);
    }

    @Override
    public void execute(CommandSender s, String[] strings) {
        if (s instanceof Player) {
            Player p = (Player) s;

            p.sendMessage("Test Worked!");
        }
    }

    @Override
    public boolean canSee(CommandSender s) {
        return false;
    }
}
