package org.magenpurp.onfimarist.ecospawners.commands;

import org.bukkit.command.CommandSender;
import org.magenpurp.api.command.SubCommand;
import org.magenpurp.onfimarist.ecospawners.Main;

public class DebuggerSubCommand extends SubCommand {

    public DebuggerSubCommand(String name, String[] permissions) {
        super(name, permissions);
    }

    @Override
    public void execute(CommandSender s, String[] args) {
        if (Main.debugger = true) Main.debugger = false;
        if (Main.debugger = false) Main.debugger = true;
    }

    @Override
    public boolean canSee(CommandSender s) {
        return false;
    }
}
