package org.magenpurp.onfimarist.ecospawners.commands;

import org.bukkit.command.CommandSender;
import org.magenpurp.api.command.ParentCommand;

public class EcoSpawnersCommand extends ParentCommand {

    public EcoSpawnersCommand() {
        super("ecospawners");
    }

    @Override
    public void sendDefaultMessage(CommandSender s) {
        s.sendMessage("");
        s.sendMessage("");
        showCommandsList(s);
    }

    @Override
    public String noPermissionMessage() {
        return null;
    }
}
