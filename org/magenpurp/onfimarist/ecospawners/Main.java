package org.magenpurp.onfimarist.ecospawners;

import net.milkbowl.vault.Vault;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.magenpurp.api.MagenAPI;
import org.magenpurp.api.versionsupport.VersionSupport;
import org.magenpurp.onfimarist.ecospawners.commands.DebuggerSubCommand;
import org.magenpurp.onfimarist.ecospawners.commands.EcoSpawnersCommand;
import org.magenpurp.onfimarist.ecospawners.commands.ReloadSubCommand;
import org.magenpurp.onfimarist.ecospawners.files.PricesFile;

public class Main extends JavaPlugin {
    private static PricesFile prices;
    private static Economy econ = null;
    public static VersionSupport versionSupport;
    public static boolean debugger;

    @Override
    public void onEnable() {
        new MagenAPI(this).initCMD().loadMagenAPI();

        debugger = true;

        EcoSpawnersCommand ecCommand = new EcoSpawnersCommand();
        ecCommand.addSubCommand(new ReloadSubCommand("reload", new String[] {"ecospawners.admin.reload"}), "Test command");
        ecCommand.addSubCommand(new DebuggerSubCommand("debug", new String[] {"ecospawners.admin.debugger"}), "Enabled or disables debugger.");

        versionSupport.registerCommand(ecCommand);
    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }

    public static PricesFile getPrices() {
        return prices;
    }

    static Economy getEconomy() {
        return econ;
    }
}


